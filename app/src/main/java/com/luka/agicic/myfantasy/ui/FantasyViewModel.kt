package com.luka.agicic.myfantasy.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class FantasyViewModel : ViewModel(){
    val db = Firebase.firestore

    val playerData = mutableListOf<Player>()
    var balanceData: MutableMap<String, Long> = mutableMapOf("balance" to 200)

    var selectedTeam: MutableMap<String, Player?> = mutableMapOf(
        "GK" to null,
        "DEF" to null,
        "RM" to null,
        "LM" to null,
        "ST" to null
    )

    init {
        fetchDatabaseData()
    }

    private fun fetchDatabaseData() {
        db.collection("players").get()
            .addOnSuccessListener { querySnapshot ->
                for (document in querySnapshot.documents) {
                    val player = document.toObject(Player::class.java)
                    player?.id = document.id

                    if (player != null)
                        playerData.add(player)
                    Log.d("aga", "${playerData.size}")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("aga", "${exception}")
            }
        db.collection("balance").document("balance").get()
            .addOnSuccessListener { document ->
                balanceData = document.data as MutableMap<String, Long>
            }
            .addOnFailureListener { exception ->
                Log.d("aga", "${exception}")
            }

        db.collection("team").document("team").get()
            .addOnSuccessListener { document ->
                selectedTeam["GK"] = document.toObject(Player::class.java)
                selectedTeam["DEF"] = document.toObject(Player::class.java)
                selectedTeam["RM"] = document.toObject(Player::class.java)
                selectedTeam["LM"] = document.toObject(Player::class.java)
                selectedTeam["ST"] = document.toObject(Player::class.java)
            }
    }

    fun updateBalance(){
        db.collection("balance").document("balance").set(balanceData)
    }

    fun updateTeam(){
        db.collection("team").document("team").set(selectedTeam)
    }

    fun resetBalance() {
        balanceData["balance"] = 200L
        updateBalance()
    }
}