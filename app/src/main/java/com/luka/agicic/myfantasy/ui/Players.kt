package com.luka.agicic.myfantasy.ui

import android.os.Bundle
import android.util.Log
import android.widget.TableRow
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ListItem
import androidx.compose.material3.Snackbar
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.luka.agicic.myfantasy.Screen
import com.luka.agicic.myfantasy.ui.theme.DarkGreen

@Composable
fun ListAllPlayers(navController: NavController, viewModel: FantasyViewModel) {
    val searchQuery = remember { mutableStateOf("") }
    val sortState = remember { mutableStateOf(false) }
    var budget = rememberSaveable { mutableStateOf(100) }
    val showSnackbar = remember { mutableStateOf(false) }
    val positionCounts = remember { mutableStateOf(mapOf<String, Int>()) }
    val showDialog = remember { mutableStateOf(false) }
    val dialogMessage = remember { mutableStateOf("") }
    val selectedPlayers = remember { mutableStateOf(listOf<Player>()) }
    val playerPrice = remember { mutableStateOf<Int?>(null) }
    val previousBudget = remember { mutableStateOf(budget.value) }  // Add this line

    val players = viewModel.playerData


    val filteredPlayers = players.filter { it.name.contains(searchQuery.value, ignoreCase = true) }
    val sortOrder = remember { mutableStateOf(0) }
    val sortedPlayers = when (sortOrder.value) {
        1 -> {
            filteredPlayers.sortedByDescending { it.price }
        }
        2 -> {
            filteredPlayers.sortedBy { it.price }
        }
        else -> filteredPlayers
    }

    Column(modifier = Modifier
        .fillMaxSize()
        .padding(16.dp)) {
        Column(  // Change this from Row to Column
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally  // Add this to center the items
        ) {
            Row(  // Add this Row to keep the buttons in the same line
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Button(onClick = { navController.navigate("createTeam/GK/Raya")
                }) {
                    Text("Create Team")
                }
                Button(
                    onClick = { sortOrder.value = (sortOrder.value + 1) % 3 },
                    colors = ButtonDefaults.buttonColors(containerColor = DarkGreen)
                ) {
                    Text("Sort by Price")
                }
            }
            TextField(  // Add this TextField
                value = searchQuery.value,
                onValueChange = { newValue -> searchQuery.value = newValue },
                modifier = Modifier
                    .padding(top = 12.dp)
                    .fillMaxWidth()
            )
            Text(
                "Budget: ${viewModel.balanceData["balance"]}$",  // Update this line to display the current budget
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(16.dp)
            )
        }
        LazyColumn {
            items(sortedPlayers) { player ->
                PlayerRow(player = player, viewModel = viewModel, onClick = { selectedPlayer ->
                    // Update the selectedPlayerName in the ViewModel
                    // viewModel.selectedPlayerName.value = selectedPlayer.name
                    if(selectedPlayer.position=="GK"){
                        viewModel.selectedTeam["GK"] = selectedPlayer
                    }
                    else if(selectedPlayer.position=="DEF") {
                        viewModel.selectedTeam["DEF"] = selectedPlayer
                    }
                    else if(selectedPlayer.position=="RM"){
                        viewModel.selectedTeam["RM"] = selectedPlayer
                    }
                    else if(selectedPlayer.position=="LM"){
                        viewModel.selectedTeam["LM"] = selectedPlayer
                    }
                    else if(selectedPlayer.position=="ST"){
                        viewModel.selectedTeam["ST"] = selectedPlayer
                    }
                    viewModel.updateTeam()
                    navController.navigate("createTeam/${selectedPlayer.position}/${selectedPlayer.name}")
                })
            }
        }
    }
}

@Composable
fun PlayerRow(player: Player, onClick: (Player) -> Unit, viewModel: FantasyViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clickable {
                val balance = viewModel.balanceData["balance"] ?: 0
                if (balance - player.price >= 0) {
                    viewModel.balanceData["balance"] = balance - player.price
                    Log.d("Aga", "Balance: ${viewModel.balanceData["balance"]} ${balance}")
                    viewModel.updateBalance()
                    onClick(player)
                }
            },
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(player.position)
        Text(player.name)
        Text(player.club)
        Text(player.price.toString())  // Convert the price to a string for display
    }
}