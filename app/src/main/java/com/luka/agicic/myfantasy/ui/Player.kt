package com.luka.agicic.myfantasy.ui

data class Player(
    var id: String = "",
    val position: String = "",
    val name: String = "",
    val club: String = "",
    val price: Int = 0
){
    operator fun plus(other: Player): Player {
        return Player(this.id, this.position, this.name + " " + other.name, this.club, this.price)
    }
}