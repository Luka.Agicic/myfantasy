package com.luka.agicic.myfantasy.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.luka.agicic.myfantasy.R
import com.luka.agicic.myfantasy.Screen


@Composable
fun CreateTeam(navController: NavController, position: String?, name: String?, viewModel: FantasyViewModel) {

    val selectedPlayerPosition = navController.previousBackStackEntry?.arguments?.getString("position")
    val selectedPlayerName = navController.previousBackStackEntry?.arguments?.getString("name")

    val players = viewModel.playerData

    Column(modifier = Modifier.fillMaxSize()) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(onClick = {
                navController.navigate(Screen.MainScreen.route)
            }) {
                Text("Main Screen")
            }
            Button(onClick = {
                navController.navigate(Screen.ListAllPlayers.route)
            }) {
                Text("List All Players")
            }
        }
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            Image(
                painter = painterResource(id = R.drawable.background),
                contentDescription = "Background Image",
                modifier = Modifier
                    .fillMaxSize()
            )
            Column(
                modifier = Modifier
                    .align(Alignment.Center)
            ) {
                // Goalkeeper
                Text(
                    viewModel.selectedTeam["GK"]?.name ?: "",
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .align(CenterHorizontally)
                        .padding(top = 10.dp)
                )
                Image(
                    painter = painterResource(id = R.drawable.gk_shirt),
                    contentDescription = "GK Shirt Image",
                    modifier = Modifier
                        .align(CenterHorizontally)
                        .width(150.dp)
                        .height(80.dp)
                        .scale(0.5f)
                )
                Text(
                        viewModel.selectedTeam["DEF"]?.name ?: "",
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .align(CenterHorizontally)
                    .padding(top = 10.dp)
                )
                Image(
                    painter = painterResource(id = R.drawable.def_shirt),
                    contentDescription = "Def Shirt Image",
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .width(150.dp)
                        .height(80.dp)
                        .scale(0.5f)
                )
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    // Midfielder 1
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            viewModel.selectedTeam["RM"]?.name ?: "",
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .align(CenterHorizontally)
                                .padding(top = 10.dp)
                        )
                        Image(
                            painter = painterResource(id = R.drawable.mid_shirt),
                            contentDescription = "Mid Shirt Image",
                            modifier = Modifier
                                .width(150.dp)
                                .height(80.dp)
                                .scale(0.5f)
                        )
                    }

                    // Midfielder 2
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            viewModel.selectedTeam["LM"]?.name ?: "",
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .align(CenterHorizontally)
                                .padding(top = 10.dp)
                        )
                        Image(
                            painter = painterResource(id = R.drawable.mid_shirt),
                            contentDescription = "Mid Shirt Image",
                            modifier = Modifier
                                .width(150.dp)
                                .height(80.dp)
                                .scale(0.5f)
                        )
                    }
                }
                // Striker
                Text(
                    viewModel.selectedTeam["ST"]?.name ?: "",
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .align(CenterHorizontally)
                        .padding(top = 20.dp)
                )
                Image(
                    painter = painterResource(id = R.drawable.st_shirt),
                    contentDescription = "St Shirt Image",
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .width(150.dp)
                        .height(80.dp)
                        .scale(0.5f)
                )
                var teamReadyText = ""
                if(viewModel.selectedTeam["GK"] != null && viewModel.selectedTeam["DEF"] != null && viewModel.selectedTeam["RM"] != null && viewModel.selectedTeam["LM"] != null && viewModel.selectedTeam["ST"] != null){
                    teamReadyText = "Your team is ready!"
                }
                else{
                    teamReadyText = "Your team is not ready!"
                }
                Text(
                    teamReadyText,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .align(CenterHorizontally)
                        .padding(top = 10.dp)
                )
            }
        }
    }
}
