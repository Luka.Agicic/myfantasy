package com.luka.agicic.myfantasy.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.unit.dp
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.luka.agicic.myfantasy.Screen

@Composable
fun MainScreen(navController: NavController, viewModel: FantasyViewModel) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        CreateTeamButton(onClick = {
            navController.navigate("createTeam/{position}/{name}")
        })
        Spacer(modifier = Modifier.height(16.dp) )
        ResetButton(onClick = {
            var selectedTeam: MutableMap<String, Player?> = mutableMapOf(
                "GK" to null,
                "DEF" to null,
                "RM" to null,
                "LM" to null,
                "ST" to null
            )
            viewModel.selectedTeam = selectedTeam
            viewModel.updateTeam()
            viewModel.resetBalance()
        })
    }
}

@Composable
fun CreateTeamButton(onClick: () -> Unit) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .fillMaxWidth(0.7f)
            .height(100.dp)
            .padding(16.dp) // Add padding around the button
            .clip(RoundedCornerShape(25.dp)), // Round the corners of the button
    ){
        Text("CREATE TEAM")
    }
}

@Composable
fun ResetButton(onClick: () -> Unit) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .fillMaxWidth(0.7f)
            .height(100.dp)
            .padding(16.dp) // Add padding around the button
            .clip(RoundedCornerShape(25.dp)), // Round the corners of the button
    ){
        Text("RESET")
    }
}
