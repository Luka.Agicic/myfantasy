package com.luka.agicic.myfantasy

import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.luka.agicic.myfantasy.ui.CreateTeam
import com.luka.agicic.myfantasy.ui.FantasyViewModel
import com.luka.agicic.myfantasy.ui.ListAllPlayers
import com.luka.agicic.myfantasy.ui.MainScreen

@Composable
fun AppNavigator(viewModel: FantasyViewModel) {
    val navController = rememberNavController()
    NavHost(navController, startDestination = "mainScreen") {
        composable("mainScreen") {
            MainScreen(navController, viewModel)
        }
        composable("listAllPlayers") {
            ListAllPlayers(navController, viewModel)
        }
        composable("createTeam/{position}/{name}") { backStackEntry ->
            val position = backStackEntry.arguments?.getString("position")
            val name = backStackEntry.arguments?.getString("name")
            CreateTeam(navController, position, name, viewModel)
        }
    }
}
