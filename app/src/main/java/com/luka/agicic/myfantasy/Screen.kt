package com.luka.agicic.myfantasy

sealed class Screen(val route: String) {
    object MainScreen : Screen("mainScreen")
    object CreateTeam : Screen("createTeam")
    object ListAllPlayers : Screen("listAllPlayers")
}